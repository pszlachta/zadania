import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;

public class Kontroler {
    private static final int ILE_LICZB_DO_WYLOSOWANIA = 4;
    private MaszynaLosujaca maszynaLosujaca;
    private Gracz gracz;
    private Dane dane;

    public Kontroler(Gracz gracz, MaszynaLosujaca maszynaLosujaca) {
        this.maszynaLosujaca = maszynaLosujaca;
        maszynaLosujaca.losowanieLiczbZPrzedzialu();
        this.gracz = gracz;
    }

    public void losujDoMomentuPoprawnegoWytypowania() throws IOException {
        int zaKtorymRazemTrafione = 1;
        for (int i = 0; i < 100; i++) {
            gracz.losowanieLiczb();
            if (czyMamyZwyciezce()) {
                zapiszZwyciezce(zaKtorymRazemTrafione);
                break;
            }
            zaKtorymRazemTrafione++;
        }

        if (!czyMamyZwyciezce()) {

            for (int i = 0; i < 1000; i++) {
                gracz.losowanieDwochOstatnichLiczb();
                if (czyMamyZwyciezce()) {
                    zapiszZwyciezce(zaKtorymRazemTrafione);
                    break;
                }
                zaKtorymRazemTrafione++;
            }
        }

        while (!czyMamyZwyciezce()) {
            gracz.losowanieOstaniejLiczby();
            if (czyMamyZwyciezce()) {
                zapiszZwyciezce(zaKtorymRazemTrafione);
            }
            zaKtorymRazemTrafione++;
        }
    }

    private void zapiszZwyciezce(int zaKtorymRazemTrafione) throws IOException {
        dane = new Dane(gracz.getWytypowaneLiczby(), zaKtorymRazemTrafione);
        dane.zapiszDaneDoPliku();
    }

    public void wyswietlLiczbyGraczaIMaszyny() {
        System.out.println("Liczby wytypowane przez gracza: ");
        System.out.println(Arrays.toString(gracz.getWytypowaneLiczby()));
        System.out.println("Liczby wylosowane przez maszyne: ");
        System.out.println(Arrays.toString(maszynaLosujaca.getWylosowaneLiczby()));
    }


    private boolean czyMamyZwyciezce() {
        int ileLiczbSieZgadza = 0;

        for (int i = 0; i < maszynaLosujaca.getWylosowaneLiczby().length; i++) {
            for (int j = 0; j < gracz.getWytypowaneLiczby().length; j++) {
                if (maszynaLosujaca.getWylosowaneLiczby()[i] == gracz.getWytypowaneLiczby()[j])
                    ileLiczbSieZgadza++;
            }
        }

        return ileLiczbSieZgadza == ILE_LICZB_DO_WYLOSOWANIA;
    }
}
