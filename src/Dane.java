import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Dane {
    private SimpleDateFormat simpleDateFormat;
    private Date date;
    private String data;
    private int[] szczesliweLiczby;
    private int zaKtorymTrafione;

    public Dane(int[] szczesliweLiczby, int zaKtorymTrafione) {
        simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        date = new Date();
        this.data = simpleDateFormat.format(date);
        this.szczesliweLiczby = szczesliweLiczby;
        this.zaKtorymTrafione = zaKtorymTrafione;
    }

    public void zapiszDaneDoPliku() throws IOException {
        FileWriter writer = new FileWriter("szczesliwyTraf.txt");
        BufferedWriter bw = new BufferedWriter(writer);

        bw.write(toString());
        bw.close();
    }

    @Override
    public String toString() {
        return "Data: " + data + "\n" +
                "Zwycięskie liczby: " + Arrays.toString(szczesliweLiczby) + "\n" +
                "Podejście nr: " + zaKtorymTrafione;
    }
}
