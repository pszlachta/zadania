import java.util.Random;

public class Gracz {
    private static int ILE_LICZB_DO_WYLOSOWANIA = 4;
    private static int LOSOWANIE_DWOCH_OSTATNICH_LICZB = 2;
    private static int LOSOWANIE_OSTATNIEJ_LICZBY = 3;
    private String pseudonim;
    private int[] wytypowaneLiczby;
    private Random random;
    private MaszynaLosujaca maszynaLosujaca;

    public Gracz(String pseudonim, MaszynaLosujaca maszynaLosujaca) {
        this.pseudonim = pseudonim;
        this.maszynaLosujaca = maszynaLosujaca;
        wytypowaneLiczby = new int[ILE_LICZB_DO_WYLOSOWANIA];
        random = new Random();
    }

    public void losowanieLiczb() {
        int index = 0;

        while (index < 4) {
            int wylosowanaLiczba = random.nextInt(29) + 1;
            if (czyLiczbaJestUnikalna(wylosowanaLiczba)) {
                wytypowaneLiczby[index] = wylosowanaLiczba;
                index++;
            }
        }
    }

    public void losowanieDwochOstatnichLiczb() {
        int index = LOSOWANIE_DWOCH_OSTATNICH_LICZB;

        uzupelnijTabliceO(LOSOWANIE_DWOCH_OSTATNICH_LICZB);

        while (index < ILE_LICZB_DO_WYLOSOWANIA) {
            int wylosowanaLiczba = random.nextInt(29) + 1;
            if (czyLiczbaJestUnikalna(wylosowanaLiczba)) {
                wytypowaneLiczby[index] = wylosowanaLiczba;
                index++;
            }
        }
    }

    public void losowanieOstaniejLiczby() {
        int index = LOSOWANIE_OSTATNIEJ_LICZBY;

        uzupelnijTabliceO(LOSOWANIE_OSTATNIEJ_LICZBY);

        while (index < ILE_LICZB_DO_WYLOSOWANIA) {
            int wylosowanaLiczba = random.nextInt(29) + 1;
            if (czyLiczbaJestUnikalna(wylosowanaLiczba)) {
                wytypowaneLiczby[index] = wylosowanaLiczba;
                index++;
            }
        }
    }

    private boolean czyLiczbaJestUnikalna(int liczba) {
        for (int l : wytypowaneLiczby) {
            if (l == liczba) {
                return false;
            }
        }
        return true;
    }

    private void uzupelnijTabliceO(int ileLiczb) {
        for (int i = 0; i < ileLiczb; i++) {
            wytypowaneLiczby[i] = maszynaLosujaca.getWylosowaneLiczby()[i];
        }
    }

    public int[] getWytypowaneLiczby() {
        return wytypowaneLiczby;
    }

}
