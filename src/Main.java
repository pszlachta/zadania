import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        MaszynaLosujaca maszynaLosujaca = new MaszynaLosujaca();
        Gracz gracz1 = new Gracz("nick1", maszynaLosujaca);

        Kontroler kontroler = new Kontroler(gracz1, maszynaLosujaca);
        kontroler.losujDoMomentuPoprawnegoWytypowania();
        kontroler.wyswietlLiczbyGraczaIMaszyny();

    }
}
