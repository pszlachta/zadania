import java.util.Random;

public class MaszynaLosujaca {
    private static int ILE_LICZB_DO_WYLOSOWANIA = 4;
    private int[] wylosowaneLiczby;
    private Random random;

    public MaszynaLosujaca() {
        random = new Random();
        wylosowaneLiczby = new int[ILE_LICZB_DO_WYLOSOWANIA];
    }

    public void losowanieLiczbZPrzedzialu() {
        int index = 0;

        while (index < ILE_LICZB_DO_WYLOSOWANIA) {
            int wylosowanaLiczba = random.nextInt(29) + 1;

            if (czyLiczbaJestUnikalna(wylosowanaLiczba)) {
                wylosowaneLiczby[index] = wylosowanaLiczba;
                index++;
            }
        }
    }

    private boolean czyLiczbaJestUnikalna(int liczba) {
        for (int l : wylosowaneLiczby) {
            if (l == liczba) {
                return false;
            }
        }
        return true;
    }

    public int[] getWylosowaneLiczby() {
        return wylosowaneLiczby;
    }
}
